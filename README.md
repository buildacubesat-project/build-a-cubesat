# Build a CubeSat Placeholder Repo

Hi! I am developing a relatively affordable, open CubeSat project and documenting my progress on [Codeberg](https://codeberg.org/buildacubesat-project) and on [YouTube](https://www.youtube.com/@buildacubesat). This here is just a placeholder repo.

Build a CubeSat placeholder repo. This project is hosted on Codeberg (https://codeberg.org/buildacubesat-project)
